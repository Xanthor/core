# Select image from https://hub.docker.com/_/php/
#image: php:7.2
# Use a prepared Hubzilla image to optimise pipeline run
image: registry.gitlab.com/dawnbreak/hubzilla/core:php7.2


stages:
  - test
  - deploy


# Select what we should cache
cache:
  paths:
  - vendor/
  - .cache/


# global variables for all jobs, if no job specific variables
variables:
  # Tell composer to use the project workspace .cache folder
  COMPOSER_CACHE_DIR: "$CI_PROJECT_DIR/.cache/composer"
  # Ignore a Composer warning
  COMPOSER_ALLOW_SUPERUSER: 1
  # Configure MySQL/MariaDB service (https://hub.docker.com/_/mysql/, https://hub.docker.com/_/mariadb/)
  MYSQL_DATABASE: hello_world_test
  MYSQL_ROOT_PASSWORD: mysql
  # Configure PostgreSQL service (https://hub.docker.com/_/postgres/)
  POSTGRES_DB: ci-db
  POSTGRES_USER: ci-user
  POSTGRES_PASSWORD: ci-pass


before_script:
# Install & enable Xdebug for code coverage reports
- pecl install xdebug
- docker-php-ext-enable xdebug
# Install composer
- curl -sS https://getcomposer.org/installer | php
# Install dev libraries from composer
- php composer.phar install --no-progress


# test PHP7 with MySQL 5.7
php7.2_mysql 1/2:
  stage: test
  services:
  - mysql:5.7
  script:
  - echo "USE $MYSQL_DATABASE; $(cat ./install/schema_mysql.sql)" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - echo "SHOW DATABASES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - echo "USE $MYSQL_DATABASE; SHOW TABLES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - vendor/bin/phpunit --configuration tests/phpunit.xml --coverage-text


# test PHP7 with MySQL latest (8)
php7.2_mysql 2/2:
  stage: test
  services:
  - name: mysql:latest
    command: ["--default-authentication-plugin=mysql_native_password"]
  script:
  - echo "USE $MYSQL_DATABASE; $(cat ./install/schema_mysql.sql)" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - echo "SHOW DATABASES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - echo "USE $MYSQL_DATABASE; SHOW TABLES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - vendor/bin/phpunit --configuration tests/phpunit.xml --coverage-text


# test PHP7 with MariaDB latest (10.3)
php7.2_mariadb:
  stage: test
  services:
  - name: mariadb:latest
    alias: mysql
  script:
  - echo "USE $MYSQL_DATABASE; $(cat ./install/schema_mysql.sql)" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - echo "SHOW DATABASES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - echo "USE $MYSQL_DATABASE; SHOW TABLES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mysql "$MYSQL_DATABASE"
  - vendor/bin/phpunit --configuration tests/phpunit.xml --coverage-text


# test PHP7 with PostgreSQL latest
php7.2_postgres:
  stage: test
  services:
  - postgres:latest
  script:
  - export PGPASSWORD=$POSTGRES_PASSWORD
  - psql --version
  - psql -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c "SELECT VERSION();"
  # Import hubzilla's DB schema
  - psql -h "postgres" -U "$POSTGRES_USER" -v ON_ERROR_STOP=1 --quiet "$POSTGRES_DB" < ./install/schema_postgres.sql
  # Show databases and relations/tables of hubzilla's database
  #- psql -h "postgres" -U "$POSTGRES_USER" -l
  #- psql -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c "\dt;"
  # Run the actual tests
  - vendor/bin/phpunit --configuration tests/phpunit-pgsql.xml --testdox
  artifacts:
    expire_in: 1 week
    # Gitlab should show the results, but has problems parsing PHPUnit's junit file.
    reports:
      junit: tests/results/junit.xml
    # Archive test results (coverage, testdox, junit)
    name: "$CI_COMMIT_REF_SLUG-$CI_JOB_NAME"
    paths:
      - tests/results/


# Generate Doxygen API Documentation and deploy it at GitLab pages
pages:
  stage: deploy
  cache: {}
  image: php:7-cli-alpine
  before_script:
    - apk update
    - apk add doxygen ttf-freefont graphviz
  script:
    - doxygen util/Doxyfile
    - mv doc/html/ public/
    - echo "API documentation should be accessible at https://hubzilla.frama.io/core/ soon"
  artifacts:
    paths:
      - public
  only:
    # Only generate it on main repo's master branch
    - master@hubzilla/core
